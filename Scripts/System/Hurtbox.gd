extends Area2D

var HitEffect = preload("res://Scenes/Effects/HitEffect.tscn")

export(Vector2) var hitEffectOffset = Vector2.ZERO
onready var timer = $Timer

#En vez de hacerlo con un bool, se hace como funcion y se llama solo cuando se necesite
# export(bool) var show_hit_effect = true

var invincible = false setget set_invincible # Función que se llamara cada vez que el valor cambie

signal invincibility_start
signal invincibility_end

func set_invincible(value):
	invincible = value
	
	if invincible:
		emit_signal("invincibility_start")
	else:
		emit_signal("invincibility_end")

func start_invincibility(time) :
	self.invincible = true
	timer.start(time)

func _on_Timer_timeout():
# Si se usa el self, se llamará al seter dentro de la misma clase, si no solo se cambia el valor
	self.invincible = false

func _on_Hurtbox_invincibility_start():
	# "Desactiva" la hitbox.
	set_deferred("monitoring", false)
	#monitorable = false
	
func _on_Hurtbox_invincibility_end():
	monitoring = true

func create_hit_effect():
	#if show_hit_effect:
	var h = HitEffect.instance()
	var world = get_tree().current_scene
	world.add_child(h)
	h.global_position = global_position + hitEffectOffset
