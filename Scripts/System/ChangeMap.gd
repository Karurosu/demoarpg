extends Area2D

export(String, "Up", "Down", "Left", "Right") var direction

signal change_map(direction)

func _on_ChangeMap_area_entered(_area):
	emit_signal("change_map", direction)
	pass
	
func _on_ChangeMap_body_entered(body):
	if body is Player :
		emit_signal("change_map", direction)
