extends KinematicBody2D
class_name Player

onready var manager = get_node("Manager") # Se encarga de lo que puede hacer y lo que no
onready var pActions = get_node("SwordPivot") # Se encarga de atacar y usar items
onready var sprite = get_node("Sprite") # Se encarga de las animaciones

# Variables de MOVIMIENTO
var velocity = Vector2.ZERO # El vector de movimiento del jugador
var inputVector = Vector2.ZERO # El vector del input
var roll_vector = Vector2.DOWN # Da igual, se actualiza cada frame

# Export es como [SerializeField], para que se vean desde editor
export var speed = 100
export var roll_multiplier = 1.33
export var acceleration = 1500
export var friction = 1200
export var swimSpeedMultiplier = 0.65

onready var hurtbox = $Hurtbox # La zona en la que recibe el daño
var hitEffect = preload("res://Scenes/Effects/PlayerHitEffect.tscn")

onready var tilemap = $"../TileMap"

# ESTADOS del jugador ////
enum { walk, attack, roll}
var playerState = walk # El estado actual

# He hecho playerStats singleton y eso lo hace accesible desde cualquier parte del codigo
var stats = PlayerStats

# Start / Awake _ready
func _ready():
	call_deferred("setTilemap")
	_connect_signals()
	sprite.animationTree.active = true
	# sword.knockback_vector =  roll_vector
	velocity = Vector2.ZERO

func setTilemap():
	if (tilemap == null) :
		tilemap = $"../TileMap"

# Update
func _physics_process(delta): # Mas o menos como el Update
	match playerState :
		walk:
			move_state(delta)
		
		attack:
			attack_state(delta)
			
		roll:
			roll_state(delta)

func checkInputAndDoAction() :
	# ATACAR
	if Input.is_action_just_pressed("attack") and manager.canAttack : 
		playerState = attack
	# RODAR
	if Input.is_action_just_pressed("roll") :
		playerState = roll
		disableWorldCollisions()
	# POLOCAR UNA BOMBA
	if Input.is_action_just_pressed("bomb") and manager.canBomb:
		pActions.place_bomb()
	# DISPARAR UNA FLECHA
	if Input.is_action_just_pressed("shoot") and manager.canShoot:
		pActions.shoot_arrow(roll_vector)

func move_state(delta):	
	# ↑ = -1 (y) || ↓ = 1 (y) || → = 1 (x) || ← = -1 (x)
	# Haciendo la resta de ambos inputs se puede saber hacia donde se dirige
	inputVector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	inputVector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	inputVector = inputVector.normalized() # Normalizar para que no se pase de 1,1
	
	if inputVector != Vector2.ZERO :
		sprite.playAnimation("Run")
		sprite.set_animation_blend_values(inputVector)
		
		velocity += inputVector * acceleration * delta
		velocity = velocity.limit_length(speed)
		roll_vector = inputVector
		pActions.sword.knockback_vector = inputVector
		
	else :
		sprite.playAnimation("Idle")
		# Para no pararlo de golpe, hacer que progresivamente se diriga al ZERO
		velocity = velocity.move_toward(Vector2.ZERO, friction * delta)
	
	if (manager.isSwimming) : velocity = move_and_slide(velocity * swimSpeedMultiplier) # Ya aplica deltaTime por defecto
	else : velocity = move_and_slide(velocity)
	
	checkInputAndDoAction()

func attack_state(_delta):
	sprite.playAnimation("Attack")

func roll_state(_delta):
	hurtbox.set_invincible(true)
	sprite.playAnimation("Roll")
	velocity = roll_vector * speed * roll_multiplier
	velocity = move_and_slide(velocity)

func _on_Hurtbox_area_entered(_area):
	# De momento todo le quita 1
	stats.health -= 1
	hurtbox.start_invincibility(0.5)
	hurtbox.create_hit_effect() # El visual
	var hit = hitEffect.instance() # El sonoro
	get_tree().current_scene.add_child(hit)

func _connect_signals():
	# Para gestionar la derrota
	stats.connect("no_health", self, "queue_free") # Cambiar a OnDeath o similar
	
	if (tilemap != null) :
		tilemap.connect("enterWater",self,"startSwim")
		tilemap.connect("exitWater",self,"stopSwim")

# Modificar con la capa adecuada para que nade. ESTO LO TENDRA EL ITEM DE LAS ALETAS
func disableWorldCollisions():
	set_collision_mask_bit(9, !get_collision_mask_bit(9))

func startSwim():
	print("Comienzo a nadar")
	sprite.changeSpriteSheet(false)
	manager.isSwimming = true

func stopSwim():
	print("Termino de nadar")
	sprite.changeSpriteSheet(true)
	manager.isSwimming = false
