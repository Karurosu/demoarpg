extends Area2D

onready var collision = $CollisionShape2D

func is_colliding():
	var areas = get_overlapping_areas()
	return areas.size() > 0
	
func get_push_vector():
	var areas = get_overlapping_areas()
	var _push_vector = Vector2.ZERO
	
	if is_colliding():
		var a = areas[0]
		return a.global_position.direction_to(self.global_position).normalized()
	
