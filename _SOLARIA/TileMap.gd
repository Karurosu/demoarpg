extends TileMap

onready var player = $"../Player"

var currentTile = ""

signal enterWater
signal exitWater

func get_tile_name(location: Vector2) -> String:
	var position := world_to_map(location)
	var tile := get_cellv(position)
	if tile == -1:
		return ""

	return tile_set.tile_get_name(tile)

func _physics_process(delta):
	var aux = get_tile_name(player.position)
	
	# Cuando toca agua desde un tile que no es agua
	if ("Water" in aux && !("Water" in currentTile) &&currentTile != aux) :
		emit_signal("enterWater")
	
	# Cuando toca tierra desde agua
	if ("Water" in currentTile && !("Water" in aux) && currentTile != aux) :
		emit_signal("exitWater")
		
	currentTile = aux
