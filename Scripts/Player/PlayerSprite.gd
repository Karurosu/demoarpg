extends Sprite

onready var player = get_parent() # El nodo padre

onready var animator = $"../AnimationPlayer" # El animator de player
onready var animationTree = $"../AnimationTree" # El arbol contiene los "blend tree"
onready var animationState = animationTree.get("parameters/playback") # Dentro del "blend tree", en cual está

onready var shadow = player.get_node("Shadow") # Para hacerlo invisible cuando nade
onready var sprite = self # Para cambiarlo cuando nade
var normalSprite = preload("res://Sprites/Player/Player.png")
var waterSprite = preload("res://Sprites/Player/Player_Swim.png")

func playAnimation(anim : String) :
	animationState.travel(anim)

func changeSpriteSheet(isWalking : bool) :
	if (isWalking) :
		sprite.texture = normalSprite
		shadow.visible = true
	else :
		sprite.texture = waterSprite
		shadow.visible = false

func set_animation_blend_values(i: Vector2) :
	animationTree.set("parameters/Idle/blend_position", i)
	animationTree.set("parameters/Run/blend_position", i)
	animationTree.set("parameters/Attack/blend_position", i)
	animationTree.set("parameters/Roll/blend_position", i)

func attack_animation_finished():
	player.playerState = player.walk

func roll_animation_finished():
	player.hurtbox.set_invincible(false)
	player.velocity /= 3
	player.playerState = player.walk
