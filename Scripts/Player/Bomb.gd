extends Node2D

onready var timer = $Timer
export var bombSeconds = 0

var explosion = preload("res://Scenes/Effects/Explosion.tscn").instance()

func _ready():
	timer.start(bombSeconds)

func _on_Timer_timeout():
	get_parent().add_child(explosion)
	explosion.position = self.position
	queue_free()
