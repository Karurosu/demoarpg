extends Node

export var max_health = 3 setget set_max_health
# Aunque se cambie en el editor, health coge el valor por defecto. Con onready coge el correcto
var health = max_health setget set_health

var coins = 0 setget set_coins

signal no_health
signal health_changed(value)
signal max_health_changed(value)
signal coins_changed(value)

func _ready():
	self.health = max_health

func set_health(value) :
	health = clamp(value, 0, max_health)
	
	emit_signal("health_changed", health)
	if health <= 0 :
		emit_signal("no_health")

func set_max_health(value):
	max_health = value
	self.health = min(health, max_health)
	emit_signal("max_health_changed", max_health)

func set_coins(value):
	coins = max(0,value)
	emit_signal("coins_changed")
