extends KinematicBody2D

onready var timer = $Timer

export var lifeSpan = 3
export var velocity = 150

var direction setget setDirection

func _ready():
	direction = Vector2.ZERO
	timer.start(lifeSpan)

func _on_Timer_timeout():
	queue_free()

func _physics_process(_delta):
	var _a = move_and_slide(direction * velocity)

func setDirection(d: Vector2) :
	direction = d

func _on_Hitbox_area_entered(_area):
	queue_free()
