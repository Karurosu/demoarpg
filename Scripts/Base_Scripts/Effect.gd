class_name Effect
extends AnimatedSprite

var rng = RandomNumberGenerator.new()

func _ready():
	
	# Como es un script que se va a reutilizar, no se puede setear manualmente
	# al evento, asi que lo setea por codigo y se adapta a todo
	var _a = self.connect("animation_finished",self,"_on_animation_finished")
	
	# Un random para flipear la animacion y que haya una pequeña variedad
	rng.randomize()
	var r = rng.randi_range(0,10)
	
	if r % 2 == 0 : flip_h = false
	else : flip_h = true
	
	play("Animation")

func _on_animation_finished():
	queue_free()
