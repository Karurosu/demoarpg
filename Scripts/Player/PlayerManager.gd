extends Node2D

# "Bloqueadores" para el progreso, que se vayan desbloqueando según se adquieren los items /
var canAttack = false
var canShoot = false
var canBomb = false
var canSwim = false # Puede nadar 
var isSwimming = false # Esta nadando ahora, para gestionar el sprite

onready var bombTimer = $"../bombTimer"
onready var arrowTimer = $"../arrowTimer"

onready var pickableItems = $"../../../PickableItems" # Referencia a todos los objetos recolectables
var pickableItemsList = []

func _ready():
	# pickableItemsList = pickableItems.get_children()
	# Para coger los objetos
	for n in range(pickableItemsList.size()) :
		var pickableItem = pickableItemsList[n]
		self.call_deferred("_connect_pickable_item_signal", pickableItem)

func enable_swim() :
	canSwim = true

func enable_bomb():
	canBomb = true
	print("Ahora puedo colorar bombas")

func enable_shoot():
	canShoot = true
	print("Ahora puedo disparar")

func enable_sword():
	canAttack = true
	print("Ahora puedo atacar")

func _on_arrowTimer_timeout():
	canShoot = true

func _on_bombTimer_timeout():
	canBomb = true

func _connect_pickable_item_signal(pObject: PickableItem):
	var _a = pObject.connect(pObject.signalName, self, pObject.functionName)
	# print("He conectado desde el manager", pObject.name)
