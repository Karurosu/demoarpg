extends Node2D

var GE_Instance = preload("res://Scenes/Effects/GrassEffect.tscn").instance()

var rng = RandomNumberGenerator.new()
export var dropChance = 15
var coin = preload("res://Scenes/World/Coin.tscn").instance()

func _on_Hurtbox_area_entered(_area): # Lo que seria OnTriggerEnter
	_destroy_grass_effect()
	_drop_item()
	queue_free()

func _destroy_grass_effect() :
	# var GrassEffect = load("res://Scenes/GrassEffect.tscn")
	# var GE_Instance = GrassEffect.instance() 

	# Spawnear en el nodo root (el primero)
	# var world = get_tree().current_scene
	# world.add_child(GE_Instance)
	
	# Como es hijo de un YSort, se puede instanciar directamente en el padre en vez del root
	# y además aprovechas para que siga estando por debajo del jugador
	get_parent().add_child(GE_Instance)
	GE_Instance.position = self.position

func _drop_item() :
	rng.randomize()	
	var r = rng.randi_range(0,100)
	
	if r < dropChance : 
		
		var p = get_parent()
		p.call_deferred("add_child", coin)
		#get_parent().add_child(coin)
		coin.position = self.position + Vector2(8,8)
