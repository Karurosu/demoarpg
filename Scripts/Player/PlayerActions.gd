extends Position2D

# Objetos para INSTANCIAR //////
var bomb = preload("res://Scenes/Player/Player_Usables/Bomb.tscn")
var arrow = preload("res://Scenes/Player/Player_Usables/Arrow.tscn")

onready var sword = $AttackHitbox # El hitbox de la espada, lo que hace daño
onready var shootPoint = $ArrowShooter # Desde donde se dispara la flecha
onready var pivot = self # El pivote respecto desde donde rota la espada y el punto de disparo

onready var player = get_parent()
onready var manager = player.get_node("Manager")

export var timeBetweenShots = 0.4
export var timeBetweenBombs = 1

func shoot_arrow(direction : Vector2) : 
	var newArrow = arrow.instance()
	get_parent().add_child(newArrow)
	newArrow.global_position = shootPoint.global_position
	newArrow.rotation_degrees = pivot.rotation_degrees + 90
	newArrow.setDirection(direction)
	manager.canShoot = false
	manager.arrowTimer.start(timeBetweenShots)
	
func place_bomb() :
	var newBomb = bomb.instance()
	player.get_parent().add_child(newBomb)
	newBomb.position = player.position
	manager.canBomb = false
	manager.bombTimer.start(timeBetweenBombs)
