extends Control

var hearts = 4 setget set_hearts
var max_hearts = 4 setget set_max_hearts

onready var fullH = $FullHearts
onready var emptyH = $EmptyHearts

func set_hearts(value):
	hearts = clamp(value, 0, max_hearts) # Limita, no será menor que 0 ni mayor que el maximo
	# Se hace con una textura que se repite cada 15 pixels, se multiplica el numero de corazones * 15
	if fullH != null: 
		fullH.rect_size.x = hearts * 15 

func set_max_hearts(value):
	# Devuelve el valor más grande entre 1 y el valor (quiere decir que nunca sera menor que 1)
	max_hearts = max(1, value)
	self.hearts = min(hearts,max_hearts)
	if emptyH != null:
		emptyH.rect_size.x = max_hearts * 15

func _ready():
	self.hearts = PlayerStats.health
	self.max_hearts = PlayerStats.max_health
	var _a = PlayerStats.connect("health_changed", self, "set_hearts")
	var _b = PlayerStats.connect("max_health_changed",self,"set_max_hearts")
