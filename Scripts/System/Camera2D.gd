extends Camera2D

signal camera_moved(direction)

func move_camera(direction):
	match direction:
		"Up": position.y -= 192
		"Down": position.y += 192
		"Left": position.x -= 320
		"Right": position.x += 320
	emit_signal("camera_moved", direction)
