extends Node2D

onready var cam = $Camera2D
onready var limitsObj = $MapLimits
var limitsList = null
onready var timer = $Timer
var limitsReplace = preload("res://Scenes/System/MapLimits.tscn")

func _ready():
	connect_limits_with_cam()
	cam.connect("camera_moved",self,"refresh_limits")

func connect_limits_with_cam():
	limitsList = limitsObj.get_children()
	
	for n in range(limitsList.size()):
		var area = limitsList[n]
		if !area.is_connected("change_map", cam, "move_camera") :
			area.connect("change_map", cam, "move_camera")

func refresh_limits(direction):
	limitsObj.queue_free()
	limitsObj = limitsReplace.instance()
	limitsList = limitsObj.get_children()
	
	# get_tree().current_scene.add_child(limitsObj)
	var w = get_tree().current_scene
	w.call_deferred("add_child",limitsObj)
	
	limitsObj.position = cam.position
	
	match direction:
		"Up": timer.start(.1)
		"Down": timer.start(.1)
		"Left": timer.start(.02)
		"Right": timer.start(.02)
		
func _on_Timer_timeout():
	pass
	connect_limits_with_cam()
