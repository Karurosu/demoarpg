extends Node2D
class_name PickableItem

export var signalName : String
export var functionName : String

# Objeto desbloqueable, que hace cuando lo coge
func _on_Area2D_area_entered(_area):
	if signalName != "" :
		emit_signal(signalName)
	queue_free()
