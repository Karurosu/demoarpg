extends KinematicBody2D

onready var stats = $Stats
onready var detectionZone = $PlayerDetection
onready var sprite = $AnimatedSprite
onready var hurtbox = $Hurtbox
onready var softcollision = $Softcollision

export var speed = 60
export var acceleration = 350
export var friction = 150

var velocity = Vector2.ZERO

var heart = preload("res://Scenes/World/Heart.tscn").instance()
var EnemyDeathEffect = preload("res://Scenes/Effects/EnemyDeathEffect.tscn").instance()
var deathEffectOffset = Vector2(0,-10)

var knockback = Vector2.ZERO
export var knockback_force = 175

enum {idle,chasing,moving}

var state = idle

func _physics_process(delta):
	
	knockback = knockback.move_toward(Vector2.ZERO, friction * delta)
	knockback = move_and_slide(knockback)
	
	match state :
		idle:
			velocity = velocity.move_toward(Vector2.ZERO, friction * delta)
			search_player()
		
		chasing:
			if detectionZone.player != null :
				var d = (detectionZone.player.global_position - self.global_position).normalized()
				velocity = velocity.move_toward(d * speed, acceleration * delta)
			
			sprite.flip_h = velocity.x < 0
			forget_player()
			
		moving:
			pass
	
	if softcollision.is_colliding():
		velocity += softcollision.get_push_vector() * delta * 400
	velocity = move_and_slide(velocity)
	
func search_player() :
	if detectionZone.can_see_player():
		state = chasing

func forget_player() :
	if !detectionZone.can_see_player():
		state = idle

#Area = con que choca = espada. Si la espada guarda el vector hacia donde se mira, se puede empujar
# al enemigo hacia ahí
func _on_Hurtbox_area_entered(area):
	knockback = area.knockback_vector * knockback_force

	# stats.set_health(stats.health - 1) 	
	stats.health -= area.damage # Si tiene el setter, se utiliza aunque no se llame
	
	hurtbox.create_hit_effect()

func _on_Stats_no_health():
	
	get_parent().add_child(EnemyDeathEffect)
	EnemyDeathEffect.global_position = global_position + deathEffectOffset
	
	#get_parent().add_child(heart)
	var p = get_parent() # Hace lo mismo, pero evita warnings
	p.call_deferred("add_child", heart)
	heart.position = self.position + deathEffectOffset
		
	queue_free()
