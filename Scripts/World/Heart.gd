extends Node2D

export var recover = 1

onready var timer = $Timer

func _on_Area2D_area_entered(_area):

	PlayerStats.health += recover
	queue_free()


func _on_Timer_timeout():
	set_deferred("monitoring", true)

func _ready():
	set_deferred("monitoring", false)
	timer.start(0.75)
