extends Node2D

onready var animatedSprite = $AnimatedSprite
export var value = 1

func _ready():
	animatedSprite.play("Coin")

func _on_Area_area_entered(_area):
	PlayerStats.coins += value
	queue_free()
