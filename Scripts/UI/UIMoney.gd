extends Control

onready var text = $Label
var money = PlayerStats

func _ready():
	var _a = PlayerStats.connect("coins_changed", self, "set_coins")
	text.text = str(money.coins)

func set_coins():
	text.text = str(money.coins)
